import akka.actor._
import scala.io._

sealed trait Message

case class Start(valor: String) extends Message
case class Msg(valor: String) extends Message




class DataSource(prox: ActorRef) extends Actor{

    override def receive: Receive = {
        case Start(valor) => {
		    println(valor)
			prox ! Msg("[DataSource]"+valor)
		}
    }

}

class LowerCase(prox: ActorRef) extends Actor{
    var msg:String = ""
    
    override def receive: Receive = {
		case Msg(valor) => {
		    msg = "[LowerCase]"+valor.toLowerCase
		    println(msg)
		    if(prox == null){
		        context.system.shutdown()
		    } else {
		        prox ! Msg(msg)
		    }
		}
    }

}

class UpperCase(prox: ActorRef) extends Actor{
    var msg:String = ""
    
    override def receive: Receive = {
		case Msg(valor) => {
		    msg = "[UpperCase]"+valor.toUpperCase
		    println(msg)
		    if(prox == null){
		        context.system.shutdown()
		    } else {
		        prox ! Msg(msg)
		    }
		}
    }

}

class FilterVowels(prox: ActorRef) extends Actor{
    var msg:String = ""
    
    override def receive: Receive = {
		case Msg(valor) => {
		    msg = "[FilterVowels]"+valor.replaceAll("[aeiou]","")
		    println(msg)
		    if(prox == null){
		        context.system.shutdown()
		    } else {
		        prox ! Msg(msg)
		    }
		}
    }

}


object DataSource{
    def main(args: Array[String]): Unit = {
        val system = ActorSystem("MainSystem")
        val lowercase = system.actorOf(Props(new LowerCase(null)),"lowercase")
        val uppercase = system.actorOf(Props(new UpperCase(lowercase)),"uppercase")
        val filtervowels = system.actorOf(Props(new FilterVowels(uppercase)),"filtervowels")
        val datasource = system.actorOf(Props(new DataSource(filtervowels)),"datasource")
        datasource ! Start("Hello")
    }
}




