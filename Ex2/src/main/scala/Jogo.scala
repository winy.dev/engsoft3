import akka.actor._
import akka.routing.RoundRobinRouter
import scala.io._
import scala.util._;

sealed trait Message

case object Iniciar extends Message
case object Jogar extends Message
case class Result(valor: Int) extends Message
case class Show(jogadores: Array[ActorRef]) extends Message

class Jogador extends Actor{
    val randnum = Random
    
    def sortear(): Int = {
        randnum.nextInt(200) + 1
    }
    
    override def receive: Receive = {
        case Jogar => sender ! Result(sortear())
    }
}

class Master(listener: ActorRef) extends Actor{
	val jogadores_num:Int = 32
    val randnum = Random
	var jogadores:Array[ActorRef] = new Array[ActorRef](0)
    var counter:Int = 0
    var n:Int = 0
 
    val jogador = context.actorOf(Props[Jogador].withRouter(RoundRobinRouter(jogadores_num)), "jogador")
      
    override def receive: Receive = {
        case Iniciar => {
			n = randnum.nextInt(200) + 1
            for(i<- 0 until jogadores_num)
                jogador ! Jogar
        }
        
        case Result(res) => {
			if(res == n){
				jogadores = sender +: jogadores
			}
            counter += 1
            if(counter == jogadores_num){
                listener ! Show(jogadores)
                //context.stop(self)
            }
        }
    }
}

class Listener extends Actor{
    def receive: Receive = {
      case Show(jogadores) ⇒
        if(jogadores.length == 0){
			println("Nenhum jogador venceu.")
		} else {
			for(i<-0 until jogadores.length){
				println("Jogador: \t%s".format(jogadores(i).path.name))
			}
		}
        context.system.shutdown()
    }
}

object Jogo{
    def main(args: Array[String]): Unit = {
        val system = ActorSystem("MainSystem")
        val listener = system.actorOf(Props[Listener], "listener")
        val masterActor = system.actorOf(Props(new Master(listener)),"masterActor")
        masterActor ! Iniciar
    }
}




