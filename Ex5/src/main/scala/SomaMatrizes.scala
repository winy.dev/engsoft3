import akka.actor._
import akka.routing.RoundRobinRouter
import scala.concurrent.duration._
import scala.io._
import java.io.File
import java.io.FileWriter

sealed trait Message

case class Somar(mat1:Array[String], mat2:Array[String], res: Array[String], start: Int, offset: Int) extends Message
case object Calculate extends Message
case object Result extends Message
case class Show(res: Array[String], workers:Int, tempo: Duration) extends Message

class SomaMatrizes extends Actor {
    
    override def receive: Receive = {
        case Somar(mat1, mat2, res, start, offset) => {
            val end = start + offset
            var str = ""

            for(i <- start until end){
                val m1 = mat1(i).split(" ")
                val m2 = mat2(i).split(" ")
                for(j <- 0 until 5000){
                    str += (m1(j).toInt + m2(j).toInt).toString + " "
                }
                res(i) = str
                str = ""
            }
            sender ! Result
        }
    }
}

class Master(mat1: Array[String], mat2: Array[String], w: Int, listener: ActorRef) extends Actor {
    var partes:Int = 0
    val size:Int = mat1.length
    //Array que guarda os resultados
    var res = new Array[String](size)
    // Calcula o número de partes para cada worker
    var p:Int = if(size%w > 0) (size/w)+1 else (size/w)
    // Guarda o tempo inicial
    val tempoInicio:Long = System.currentTimeMillis
    val soma = context.actorOf(Props[SomaMatrizes].withRouter(RoundRobinRouter(w)))
      
    override def receive: Receive = {
        case Calculate => {
            for(i <- 0 until size by w)
                soma ! Somar(mat1, mat2, res, i, if(i+w >= size) (size - i)  else w )
        }
        
        case Result => {
            partes += 1
            if(partes == p)
                listener ! Show(res, w, (System.currentTimeMillis-tempoInicio).millis)
        }
    }
}

class Listener extends Actor{

    def gravarResultado(res: Array[String]):Unit={
        //Grava o resultado no arquivo resultado
        val w = new FileWriter(new File("src/main/files/resultado"))
        w.write(res.mkString("\n"))
        w.close()
    }

    def gravarTempo(workers: Int, tempo: Duration):Unit={
        //Adiciona uma nova linha ao arquivo que guarda o tempo gasto
        val w = new FileWriter(new File("src/main/files/tempo"),true)
        w.write(s"Número de escravos: $workers\t\tTempo gasto: $tempo\n")
        w.close()
    }

    def receive: Receive = {
        case Show(res, workers, tempo) => {
            gravarResultado(res)
            gravarTempo(workers,tempo)
            context.system.shutdown()
        }
    }
}

object SomaMatrizes{
    def main(args: Array[String]): Unit = {

        //Obtendo o conteúdo dos arquivos das matrizes
        val mat1File = Source.fromFile("src/main/files/mat1")
        val mat2File = Source.fromFile("src/main/files/mat2")
        val mat1 = mat1File.getLines.toArray
        val mat2 = mat2File.getLines.toArray

        //Apaga o arquivo que guarda o tempo gasto, caso ele já exista
        val f = new File("src/main/files/tempo")
        if(f.exists())
            f.delete()

        for(i <- 1 until 513){
            val system = ActorSystem("MainSystem")
            val listener = system.actorOf(Props[Listener], "listener")
            val masterActor = system.actorOf(Props(new Master(mat1, mat2, i, listener)),"masterActor")
            masterActor ! Calculate
        }

        mat1File.close()
        mat2File.close()

    }
}




