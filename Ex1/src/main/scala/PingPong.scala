import akka.actor._
import scala.io._

sealed trait Message

case object Start extends Message
case class Ping(valor: Int) extends Message
case class Pong(valor: Int) extends Message

class AtorA(actB: ActorRef) extends Actor{
	override def receive: Receive = {
        case Start => {
			actB ! Ping(0)
		}
		
        case Pong(p: Int) => {
			if(p<2000) {
				sender ! Ping(p+1)
			} else {
				println("ATOR A")
				context.system.shutdown()
			}
		}
    }
}
class AtorB() extends Actor{
	override def receive: Receive = {
        case Ping(p: Int) => {
			if(p<2000) {
				sender ! Pong(p+1)
			} else {
				println("ATOR B")
				context.system.shutdown()
			}
		}
    }
}

object PingPong{
    def main(args: Array[String]): Unit = {
        val system = ActorSystem("MainSystem")
        val atorB = system.actorOf(Props[AtorB], "atorB")
        val atorA = system.actorOf(Props(new AtorA(atorB)),"atorA")
        atorA ! Start
    }
}




