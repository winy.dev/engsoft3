import akka.actor._
import akka.routing.RoundRobinRouter
import scala.concurrent.duration._
import scala.io._

sealed trait Message

case object Calculate extends Message
case class Calcular(start: Int, num: Int) extends Message
case class Result(valor: Int) extends Message
case class Show(resultado: Int, tempo: Duration) extends Message

class Fatorial extends Actor{
    
    def calc(start: Int, num: Int): Int = {
        var res:Int = 1
        val end:Int = start+num

        for(i <- start until end)
            if(i >= 1)
                res *= i
        return res
    }
    
    override def receive: Receive = {
        case Calcular(start,num) => sender ! Result(calc(start,num))
    }
}

class Master(f: Int, w: Int, listener: ActorRef) extends Actor{
    var resultado:Int = 1
    var counter:Int = 0
    var p:Int = if(f%w > 0) (f/w)+1 else (f/w)
    val tempoInicio: Long = System.currentTimeMillis
    val fatorial = context.actorOf(Props[Fatorial].withRouter(RoundRobinRouter(w)), "fatorial")
      
    override def receive: Receive = {
        case Calculate => {
            if(f==1)
                fatorial ! Calcular(1,f)

            for(i<- 0 until f by w)
                fatorial ! Calcular(i, if(i+w >= f) (f - i +1)  else w )
        }
        
        case Result(res) => {
            resultado *= res
            counter += 1
            if(counter == p){
                listener ! Show(resultado,(System.currentTimeMillis-tempoInicio).millis)
            }
        }
    }
}

class Listener extends Actor{
    def receive: Receive = {
      case Show(resultado, tempo) ⇒
        println("O resultado é: %s\nTempo de cálculo: %s".format(resultado, tempo))
        context.system.shutdown()
    }
}

object Fatorial{
    def main(args: Array[String]): Unit = {
        val system = ActorSystem("MainSystem")
        println("Digite o numero: ")
        val f = StdIn.readInt()
        println("Digite o número de workers: ")
        val p = StdIn.readInt()
        val listener = system.actorOf(Props[Listener], "listener")
        val masterActor = system.actorOf(Props(new Master(f, p, listener)),"masterActor")
        masterActor ! Calculate
    }
}




